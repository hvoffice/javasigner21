﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JavaSigner21
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());
        }
    }

    public class Globals
    {
        // --- paths ---
        public static string UpdaterPath    = AppDomain.CurrentDomain.BaseDirectory;
        public static string InstallPath    = @"C:\Servoy";
        public static string BackupPath     = AppDomain.CurrentDomain.BaseDirectory + "backup\\";
        public static string TempPath       = AppDomain.CurrentDomain.BaseDirectory + "temp\\";
        public static string DataFile       = "";
        // --- checking bools ---
        public static bool pathOkay         = false;
        public static bool installOkay      = false;
        public static bool backupOkay       = false;
        public static bool unzipOkay        = false;
        // --- log data ---
        public static string logFile        = AppDomain.CurrentDomain.BaseDirectory + "log_";
        public static string logData        = "";
    }
}
