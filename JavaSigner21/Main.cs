﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.ServiceProcess;
using System.Diagnostics;
using System.Threading;
using System.IO.Compression;
using System.Management;
using System.Management.Instrumentation;

namespace JavaSigner21
{
    public partial class Main : Form
    {
        #region // --- start routine ---
        public Main()
        {
            InitializeComponent();

            appendToLog("=starting the application=");
            lbl_val_path.Text = Globals.InstallPath;

            #region // --- check the servoy folder ---

            // --- check if the folder exist ---
            //if (!Directory.Exists(Globals.InstallPath))
            //{
            //    Globals.InstallPath = "ERROR";
            //    Globals.pathOkay = false;
            //    MessageBox.Show("Der Servoy-Installationsordner konnte nicht gefunden werden\nWählen Sie den Ordner von Hand aus\nKlicken Sie hierbei auf ERROR", "Fehler",
            //    MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    appendToLog("-cant find servoy folder-");
            //}
            //else
            //{
            //    appendToLog("-found servoy folder-");
            //    if (!checkServoyService())
            //    {
            //        MessageBox.Show("Der Servoy-Installationsordner ist falsch\nWählen Sie den Ordner von Hand aus\nKlicken Sie hierbei auf ERROR", "Fehler",
            //        MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        appendToLog("-cant find servoy-service in folder-");
            //    }
            //}


            #region // --- get the servoy path trough the servoy-service ----
            // --- check if servoy-service is installed on this pc ---
            ServiceController ctl = ServiceController.GetServices().FirstOrDefault(s => s.ServiceName == "ServoyService");
            if (ctl == null)
            {
                appendToLog("-cant find servoy-service-");
                btn_start.Enabled = false;
                Globals.InstallPath = "ERROR";
                Globals.pathOkay = false;
                MessageBox.Show("Der Servoy-Installationsordner konnte nicht gefunden werden\nWurde der Signer auf dem Server und als Administrator ausgeführt?", "Fehler",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                appendToLog("-cant find servoy folder-");
            }
            else
            {
                string ServiceName = "ServoyService";
                using (ManagementObject wmiService = new ManagementObject("Win32_Service.Name='" + ServiceName + "'"))
                {
                    wmiService.Get();
                    string currentserviceExePath = wmiService["PathName"].ToString();
                    string ServicePathFull = wmiService["PathName"].ToString();
                    string ServicePath = "";

                    // --- Path parsen --- example: C:\Servoy\application_server\service\wrapper.exe -s C:\Servoy\application_server\service\wrapper.conf
                    int lnPos = ServicePathFull.IndexOf("-s");

                    // ServicePath = ServicePathFull.Substring(lnPos + 2);     //  C:\Servoy\application_server\service\wrapper.conf
                    ServicePath = ServicePathFull.Remove(0, lnPos + 2);

                    ServicePath = ServicePath.Replace("\\application_server\\service\\wrapper.conf", "");
                    Globals.InstallPath = ServicePath.Trim();
                    lbl_val_path.Text = Globals.InstallPath;

                    Globals.pathOkay  = true;

                    appendToLog("-found servoy folder-");
                    appendToLog(Globals.InstallPath);
                }
            }
            #endregion




            appendToLog("-found servoy-service in folder-");
            #endregion

            #region // --- check if the installation is correct ---
            appendToLog("-start checking the .zip data-");
            if (!checkZip())
            {
                Globals.installOkay = false;
                MessageBox.Show("Die Installation ist unvollständig\nInstallieren Sie den Signer neu oder\nkontaktieren Sie HV-OFFICE", "Fehler",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                appendToLog("-cant verify the .zip data-");
            }
            appendToLog("-.zip data is okay-");
            #endregion

        }
        #endregion
        
        #region // --- pathselect from user ---
        private void lbl_val_path_Click(object sender, EventArgs e)
        {
            // --- pathselect ---
            appendToLog("-starting user path select-");
            var dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            if (dialog.ShowDialog().GetValueOrDefault())
            {
                Globals.InstallPath = dialog.SelectedPath;
                appendToLog("-user selected path: " + dialog.SelectedPath +  "-");
                checkServoyService();
            }
        }
        #endregion

        #region // --- check if the servoy-service exist ---
        public bool checkServoyService()
        {

            var ServoyService = Globals.InstallPath + "\\application_server\\service\\wrapper.jar";
            if (!File.Exists(ServoyService))
            {
                Globals.InstallPath = "ERROR";
                lbl_val_path.Text = Globals.InstallPath;
                lbl_val_path.ForeColor = System.Drawing.Color.Red;
                Globals.pathOkay = false;

                btn_start.Enabled = false;


                return false;
            }
            else
            {
                lbl_val_path.Text = Globals.InstallPath;
                lbl_val_path.ForeColor = System.Drawing.Color.Green;
                Globals.pathOkay = true;
                btn_start.Enabled = true;
                return true;
            }
            
        }
        #endregion

        #region // --- check if the .zip exist //
        public bool checkZip()
        {
            var ZipFile = Globals.UpdaterPath + "data\\signed.zip";

            if (!File.Exists(ZipFile))
            {
                lbl_val_status.Text         = "Installation unvollständig - Fehler - ";
                lbl_val_status.ForeColor    = System.Drawing.Color.Red;
                Globals.installOkay         = false;
                btn_start.Enabled           = false;


                return false;
            }
            else
            {
                Globals.DataFile            = ZipFile;
                lbl_val_status.Text         = ".zip Datei wurde geprüft - OK - ";
                lbl_val_status.ForeColor    = System.Drawing.Color.Green;
                Globals.installOkay         = true;
                btn_start.Enabled           = true;
                return true;
            }
        }
        #endregion

        #region // --- exit application ---
        private void btn_close_Click(object sender, EventArgs e)
        {
            appendToLog("=closing the application=");
            saveLog();
            Environment.Exit(0);
        }
        #endregion

        #region // --- make the form moveable --- //

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        private void panel_top_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
            appendToLog("-form moved-");
        }
        #endregion

        #region // --- start the signer ---
        private void btn_start_Click(object sender, EventArgs e)
        {
            #region // --- stop the servoy service ---                  
            appendToLog("-starting the signing process-");

            if (!serviceFuntions("stop"))
            {
                MessageBox.Show("Der Servoy Dienst konnte nicht gestoppt werden\nLäuft der Signer auf dem Server?", "Fehler",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            #endregion              

            #region // --- backup the whole servoy system ---
            appendToLog("-starting the backup system-");
            if (!startBackup())
            {
                MessageBox.Show("Es konnte kein Backup erstellt werden\nPrüfen Sie die Einstellungen", "Fehler",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            #endregion

            #region // --- unzip the signed.zip ---
            appendToLog("-starting the extract system-");
            if (!unzipData())
            {
                MessageBox.Show("Signed.zip konnte nicht enpackt werden\nSigner als Admin ausgeführt?", "Fehler",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            #endregion

            #region // --- replace all .jar files in productiv system ---
            appendToLog("-starting the .jar signing system-");
            if (!replaceJars())
            {
                MessageBox.Show("Konnte .jar Dateien nicht signen", "Fehler",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            #endregion

            #region // --- start the servoy service ---  
            appendToLog("-starting the servoy-service-");
            if (!serviceFuntions("start"))
            {
                MessageBox.Show("Der Servoy Dienst konnte nicht gestoppt werden\nLäuft der Signer auf dem Server?", "Fehler",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            #endregion

            #region // --- finish the signer ---
            MessageBox.Show("Die .jar Dateien wurden erfolgreich gesigned", "Fehler",
               MessageBoxButtons.OK, MessageBoxIcon.Information);
            appendToLog("-all system finished-");
            lbl_val_status.Text = "Signing erfolgreich!";
            btn_start.Enabled = false;
           // saveLog();
            #endregion

        }
        #endregion


        #region // --- backup function ---
        private bool startBackup()
        {
            #region // --- create backupfolder ---
            appendToLog("-checking the backup path-");
            if (Directory.Exists(Globals.BackupPath))
                {
                    Directory.Delete(Globals.BackupPath, true);
                    appendToLog("-deleted old backup path-");
            }

            Directory.CreateDirectory(Globals.BackupPath);
            appendToLog("-created the backup path-");
            #endregion

            #region // --- backup the data ---
           
            try
                {
                    // - create all of the directories -
                    prg_status.Maximum = Directory.GetDirectories(Globals.InstallPath, "*", SearchOption.AllDirectories).Length;
                    prg_status.Value = 0;
                    lbl_val_status.Text = "Erstelle Directory...";
                    appendToLog("-creating directories-");
                    foreach (string dirPath in Directory.GetDirectories(Globals.InstallPath, "*", SearchOption.AllDirectories))
                    {
                        Directory.CreateDirectory(dirPath.Replace(Globals.InstallPath, Globals.BackupPath));
                        appendToLog("-creating: "+ dirPath.Replace(Globals.InstallPath, Globals.BackupPath) + "-");
                        prg_status.Value += 1;
                        Application.DoEvents();
                    }

                    // - copy the files -
                    prg_status.Maximum = Directory.GetFiles(Globals.InstallPath, "*.*", SearchOption.AllDirectories).Length;
                    prg_status.Value = 0;
                    lbl_val_status.Text = "Erstelle Backup...";
                    appendToLog("-copy the files-");
                    foreach (string newPath in Directory.GetFiles(Globals.InstallPath, "*.*", SearchOption.AllDirectories))
                    {
                        File.Copy(newPath, newPath.Replace(Globals.InstallPath, Globals.BackupPath), true);
                        appendToLog("-copy file:" + newPath.Replace(Globals.InstallPath, Globals.BackupPath) + "-");
                        prg_status.Value += 1;
                        lbl_val_status.Text = "Backup: " + newPath.Replace(Globals.InstallPath, Globals.BackupPath);
                        Application.DoEvents();
                    }
                    Globals.backupOkay = true;
                    appendToLog("-backup process finished-");
                    return true;
                }
                catch (Exception)
                {
                    appendToLog("-backup process failed-");
                    return false;
                    throw;
                }
            #endregion
        }
        #endregion

        #region // --- service functions ---
        private bool serviceFuntions(string command)
        {
            Boolean llReturn = false;
            appendToLog("-trying to find the servoy-service-");
            ServiceController ctl = ServiceController.GetServices().FirstOrDefault(s => s.ServiceName == "ServoyService");
            if (ctl == null)
            {
                appendToLog("-cant find servoy-service-");
                return false;
            }
            appendToLog("-found servoy-service-");
            ServiceController sc = new ServiceController("ServoyService");

            // --- start the service ---
            if (command == "start")
            {
                appendToLog("-trying to start the servoy-service-");
                if ((sc.Status.Equals(ServiceControllerStatus.Stopped)) || (sc.Status.Equals(ServiceControllerStatus.StopPending)))
                {
                    // Start the service if its status is set to "Stopped".
                    lbl_val_status.Text = "Starte Servoy-Dienst...";
                    sc.Start();
                    sc.WaitForStatus(ServiceControllerStatus.Running);
                    llReturn = true;
                    appendToLog("-started the servoy-service-");
                }
                else
                {
                    appendToLog("-cant start the servoy-serivce-");
                    llReturn = true;
                }
            }

            // --- stop the service ---

            if (command == "stop")
            {
                appendToLog("-trying to stop the servoy-service-");
                if ((sc.Status.Equals(ServiceControllerStatus.Stopped)) || (sc.Status.Equals(ServiceControllerStatus.StopPending)))
                {
                    llReturn = true;
                    appendToLog("-servoy service is already stopped-");
                }
                else
                {
                    // Stop the service if its status is not set to "Stopped".
                    lbl_val_status.Text = "Stoppe Servoy-Dienst...";
                    sc.Stop();
                    sc.WaitForStatus(ServiceControllerStatus.Stopped);
                    llReturn = true;
                    appendToLog("-stopped the servoy-service-");
                }
            }
            appendToLog("-end of servoy-service functions-");
            return llReturn;
        }
        #endregion

        #region // --- unzip the signed.zip --
        private bool unzipData()
        {
            Boolean llReturn = false;
            lbl_val_status.Text = "entpacke Signed.zip...";
            Application.DoEvents();
            // --- In den Sourcepfad die Servoy20.zip entpacken ---
            string SourcePath = Globals.TempPath;
            string SignedZip = Globals.DataFile;

            if (!Directory.Exists(SourcePath))
            {
                Directory.CreateDirectory(SourcePath);
            }

            try
            {
                ZipFile.ExtractToDirectory(SignedZip, SourcePath);
                appendToLog("-extracted the data to: " + SourcePath + "-");
            }
            catch
            {
                llReturn = false;
                appendToLog("-error extracting the data-");
            }

            Globals.TempPath = Globals.TempPath + "Servoy";
            llReturn = true;


            return llReturn;
        }
        #endregion

        #region // --- replace all .jar files in productive system ---
        private bool replaceJars()
        {
            // - copy the jar files -
            prg_status.Maximum = Directory.GetFiles(Globals.TempPath, "*.jar", SearchOption.AllDirectories).Length;
            prg_status.Value = 0;
            lbl_val_status.Text = "Signe .jar Dateien...";

            foreach (string newPath in Directory.GetFiles(Globals.TempPath, "*.jar", SearchOption.AllDirectories))
            {
                File.Copy(newPath, newPath.Replace(Globals.TempPath, Globals.InstallPath), true);
                prg_status.Value += 1;
                lbl_val_status.Text = "Signing: " + newPath.Replace(Globals.TempPath, Globals.InstallPath);
                appendToLog("-signing: " + newPath.Replace(Globals.TempPath, Globals.InstallPath) + "-");
                Application.DoEvents();
            }
            Globals.backupOkay = true;
 
            return true;
        }
        #endregion

        #region // --- append to log ---
        private bool appendToLog(string text)
        {
            Globals.logData = Globals.logData + GetTimestamp(DateTime.Now) + text + "\n";
            return true;
        }
        #endregion


        #region // --- save the log ---
        private bool saveLog()
        {
            File.WriteAllText(Globals.logFile + GetTimestampShort(DateTime.Now) + ".txt", Globals.logData);
            return true;
        }
        #endregion

        #region // --- func-biblio ---
        private static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyy.MM.dd.HH:mm:ss:fff");
        }

        private static String GetTimestampShort(DateTime value)
        {
            return value.ToString("yyyy_MM_dd_HH_mm");
        }
        #endregion

    }
}
