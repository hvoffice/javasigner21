﻿namespace JavaSigner21
{
    partial class Main
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.panel_top = new System.Windows.Forms.Panel();
            this.lbl_txt_title = new System.Windows.Forms.Label();
            this.btn_close = new System.Windows.Forms.Button();
            this.lbl_val_path = new System.Windows.Forms.Label();
            this.lbl_txt_path = new System.Windows.Forms.Label();
            this.btn_start = new System.Windows.Forms.Button();
            this.lbl_txt_version = new System.Windows.Forms.Label();
            this.panel_botom = new System.Windows.Forms.Panel();
            this.prg_status = new System.Windows.Forms.ProgressBar();
            this.lbl_val_status = new System.Windows.Forms.Label();
            this.lbl_val_version = new System.Windows.Forms.Label();
            this.panel_top.SuspendLayout();
            this.panel_botom.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_top
            // 
            this.panel_top.Controls.Add(this.lbl_txt_title);
            this.panel_top.Controls.Add(this.btn_close);
            this.panel_top.Controls.Add(this.lbl_val_path);
            this.panel_top.Controls.Add(this.lbl_txt_path);
            this.panel_top.Controls.Add(this.btn_start);
            this.panel_top.Controls.Add(this.lbl_val_version);
            this.panel_top.Controls.Add(this.lbl_txt_version);
            this.panel_top.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_top.Location = new System.Drawing.Point(0, 0);
            this.panel_top.Name = "panel_top";
            this.panel_top.Size = new System.Drawing.Size(381, 155);
            this.panel_top.TabIndex = 0;
            this.panel_top.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_top_MouseDown);
            // 
            // lbl_txt_title
            // 
            this.lbl_txt_title.AutoSize = true;
            this.lbl_txt_title.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_txt_title.Location = new System.Drawing.Point(12, 9);
            this.lbl_txt_title.Name = "lbl_txt_title";
            this.lbl_txt_title.Size = new System.Drawing.Size(145, 23);
            this.lbl_txt_title.TabIndex = 6;
            this.lbl_txt_title.Text = "Java Signer 21";
            // 
            // btn_close
            // 
            this.btn_close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_close.Location = new System.Drawing.Point(276, 84);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(102, 31);
            this.btn_close.TabIndex = 5;
            this.btn_close.Text = "Schließen";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // lbl_val_path
            // 
            this.lbl_val_path.AutoSize = true;
            this.lbl_val_path.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.lbl_val_path.Location = new System.Drawing.Point(12, 57);
            this.lbl_val_path.Name = "lbl_val_path";
            this.lbl_val_path.Size = new System.Drawing.Size(39, 19);
            this.lbl_val_path.TabIndex = 4;
            this.lbl_val_path.Text = "error";
            this.lbl_val_path.Click += new System.EventHandler(this.lbl_val_path_Click);
            // 
            // lbl_txt_path
            // 
            this.lbl_txt_path.AutoSize = true;
            this.lbl_txt_path.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.lbl_txt_path.Location = new System.Drawing.Point(12, 36);
            this.lbl_txt_path.Name = "lbl_txt_path";
            this.lbl_txt_path.Size = new System.Drawing.Size(91, 19);
            this.lbl_txt_path.TabIndex = 3;
            this.lbl_txt_path.Text = "Servoy-Pfad";
            // 
            // btn_start
            // 
            this.btn_start.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_start.Location = new System.Drawing.Point(4, 121);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(374, 31);
            this.btn_start.TabIndex = 2;
            this.btn_start.Text = "Start";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // lbl_txt_version
            // 
            this.lbl_txt_version.AutoSize = true;
            this.lbl_txt_version.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.lbl_txt_version.Location = new System.Drawing.Point(294, 9);
            this.lbl_txt_version.Name = "lbl_txt_version";
            this.lbl_txt_version.Size = new System.Drawing.Size(67, 19);
            this.lbl_txt_version.TabIndex = 0;
            this.lbl_txt_version.Text = "VERSION";
            // 
            // panel_botom
            // 
            this.panel_botom.Controls.Add(this.lbl_val_status);
            this.panel_botom.Controls.Add(this.prg_status);
            this.panel_botom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_botom.Location = new System.Drawing.Point(0, 161);
            this.panel_botom.Name = "panel_botom";
            this.panel_botom.Size = new System.Drawing.Size(381, 54);
            this.panel_botom.TabIndex = 1;
            // 
            // prg_status
            // 
            this.prg_status.Location = new System.Drawing.Point(4, 24);
            this.prg_status.Name = "prg_status";
            this.prg_status.Size = new System.Drawing.Size(374, 23);
            this.prg_status.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.prg_status.TabIndex = 0;
            // 
            // lbl_val_status
            // 
            this.lbl_val_status.AutoSize = true;
            this.lbl_val_status.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.lbl_val_status.Location = new System.Drawing.Point(12, 2);
            this.lbl_val_status.Name = "lbl_val_status";
            this.lbl_val_status.Size = new System.Drawing.Size(208, 19);
            this.lbl_val_status.TabIndex = 1;
            this.lbl_val_status.Text = global::JavaSigner21.Properties.Settings.Default.status;
            // 
            // lbl_val_version
            // 
            this.lbl_val_version.AutoSize = true;
            this.lbl_val_version.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.lbl_val_version.Location = new System.Drawing.Point(313, 30);
            this.lbl_val_version.Name = "lbl_val_version";
            this.lbl_val_version.Size = new System.Drawing.Size(29, 19);
            this.lbl_val_version.TabIndex = 1;
            this.lbl_val_version.Text = "1.2";
            // 
            // Main
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.ClientSize = new System.Drawing.Size(381, 215);
            this.Controls.Add(this.panel_botom);
            this.Controls.Add(this.panel_top);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "JavaSigner21";
            this.panel_top.ResumeLayout(false);
            this.panel_top.PerformLayout();
            this.panel_botom.ResumeLayout(false);
            this.panel_botom.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_top;
        private System.Windows.Forms.Label lbl_val_version;
        private System.Windows.Forms.Label lbl_txt_version;
        private System.Windows.Forms.Panel panel_botom;
        private System.Windows.Forms.Label lbl_val_path;
        private System.Windows.Forms.Label lbl_txt_path;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Label lbl_val_status;
        private System.Windows.Forms.ProgressBar prg_status;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.Label lbl_txt_title;
    }
}

